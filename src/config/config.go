package config

import (
	"encoding/json"
	"fmt"
	"os"
)

/////////////////////////////////
// Config structures & vars
/////////////////////////////////
type Configs struct {
	Elastic  ElasticConfig   `json:"elastic"`
}

type ElasticConfig struct {
	ConnectionUrl []string `json:"connection_url"`
}

var AllConfigs *Configs

/////////////////////////////////
// Config functions
/////////////////////////////////

// Get configs
func GetConfigs() error {

	var filePath string

	if os.Getenv("config") != "" {
		filePath = os.Getenv("config")
	} else {
		currentDir, err := os.Getwd()
		if err != nil {
			fmt.Println("get current dir err: ", err)
			os.Exit(1)
		}

		filePath = currentDir + "/src/config/config.json"
	}

	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&AllConfigs)
	if err != nil {
		return err
	}

	return nil
}

