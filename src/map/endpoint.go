package _map

import "gitlab.com/baibolatovads/map/src/domain"

type CreateTargetRequest struct{
	domain.Target
}

type CreateTargetResponse struct{
	TargetId string `json:"target_id"`
}

type GetTargetRequest struct{
	TargetId string `json:"target_id"`
}

type GetTargetResponse struct{
	domain.Target
}

type MarkDoneRequest struct{
	TargetId string `json:"target_id"`
}

type MarkDoneResponse struct{
	domain.Target
}

type DeleteTargetRequest struct{
	TargetId string `json:"target_id"`
}

type DeleteTargetResponse struct{
	Msg string `json:"msg"`
}
