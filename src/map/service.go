package _map

import "gitlab.com/baibolatovads/map/src/domain"

type Service interface {
	CreateTarget(req *CreateTargetRequest) (*CreateTargetResponse, error)
	GetTarget(req *GetTargetRequest) (*GetTargetResponse, error)
	MarkDone(req *MarkDoneRequest) (*MarkDoneResponse, error)
	DeleteTarget(req *DeleteTargetRequest) (*DeleteTargetResponse, error)
}

type service struct{
	targetCommandRepo domain.TargetCommandRepo
}

func NewService(targetCR domain.TargetCommandRepo) Service{
	return &service{
		targetCommandRepo: targetCR,
	}
}

//func (s * service) CreateTarget(req *CreateTargetRequest) (*CreateTargetResponse, error){
//	target := req.Target
//	if target.Id == ""{
//		target.GenerateId()
//	}
//
//	err := s.targetCommandRepo.Create
//}