package elasticDB

import (
	"context"
	"encoding/json"
	"gitlab.com/baibolatovads/map/src/domain"
	"gitlab.com/baibolatovads/map/src/errors"
	"gopkg.in/olivere/elastic.v5"
)

type TargetCommandRepo struct{
	client *elastic.Client
}

func NewTargetCommandRepo(client *elastic.Client) domain.TargetCommandRepo{
	return &TargetCommandRepo{client: client}
}

func (r *TargetCommandRepo) Create(target *domain.Target) error{
	_, err := r.client.Index().
		Index("map").
		Type("target").
		Id(target.Id).
		BodyJson(target).
		Refresh("true").
		Do(context.TODO())

	return err
}

func (r *TargetCommandRepo) GetById(id string) (*domain.Target, error) {
	get, err := r.client.Get().
		Index("map").
		Id(id).
		Do(context.TODO())

	if err != nil {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, errors.NoContentFound
	}

	var target domain.Target

	if get.Found {
		err := json.Unmarshal(get.Source, &target)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, err
	}
	return &target, err
}


func (r *TargetCommandRepo) DeleteById(id string) error {
	_, err := r.client.Delete().
		Index("map").
		Type("target").
		Id(id).
		Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}

func (r * TargetCommandRepo) MarkDone(target *domain.Target) error{
	_, err := r.client.Update().
		Index("map").
		Type("target").
		Id(target.Id).
		Script(elastic.NewScriptInline("ctx._source.status = params.status").
			Lang("painless").
			Param("status", true)).
			Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}