package elasticDB

import(
	"gopkg.in/olivere/elastic.v5"
	"gitlab.com/baibolatovads/map/src/errors"
	"gitlab.com/baibolatovads/map/src/config"
)

func ElasticConnectionStart() (*elastic.Client, error) {

	client, err := elastic.NewClient(elastic.SetURL(config.AllConfigs.Elastic.ConnectionUrl...), elastic.SetSniff(true))
	if err != nil {
		return nil, errors.ElasticConnectError.DevMessage(err.Error())
	}

	return client, nil
}
