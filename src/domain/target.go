package domain

import "github.com/google/uuid"

type Target struct{
	Id string `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Status bool `json:"status"`
}

func (target *Target) GenerateId(){
	target.Id = uuid.New().String()
}

type TargetCommandRepo interface {
	Create(target *Target) error
	GetById(id string) (*Target, error)
	DeleteById(id string) error
	MarkDone(target *Target) error
}
